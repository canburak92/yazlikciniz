$(document).ready(function () {
    $('input[name="searchcategories"]')
    .focusin(function () {
        $(".popoverWrap").removeClass("block");
        $(".popoverWrap.categoriesField").addClass("block");
    });
    $("input[name='categories']").on("change", function () {
        var destination = [];
        $.each($("input[name='categories']:checked"), function () {
            destination.push($(this).data("label"));
        });
        $('input[name="searchcategories"]').val(destination.join(", "));
    });
    $(".popoverWrap .popoverCloseBtn").click(function() {
        $(".popoverWrap").removeClass("block");
    });
 
   $(".detailSearchBtn").on("click", function(){
		if($(this).text()=="X")
		{
			$(this).text("Detaylı Arama");
		} else {
			$(this).text("X");
		}
		$("#detailSearch").toggle(); 
			
		return false;
	});
   
    $(".searchOpenBtn").click(function() {
        $("body").addClass("noScroll");
        $("#searchBox").removeAttr("style");
    });
    $("#searchBox .closeBtn").click(function() {
        $("body").removeClass("noScroll");
        $("#searchBox").hide();
    });
    $("#searchResult .resultItem .img .galleryBtn").click(function() {
        $("#photoGallery").show();
        $(".photoGallery li.selected-img a").trigger("click");
    });
    $(".showAllCommentsBtn").click(function() {
        $(this).hide();
        $("#comment").show();
    });
	$(".afterCalculatedBtn").click(function() {
		$("#afterCalculated").show();
	});
});

$(window).on('load resize', function () {
    var genislik = $(window).width();
    if (genislik < 767) {
        $("ul.viewTypes li.gridBtn").trigger('click');
    }
    var searchHeight = $(".searchBox").outerHeight();
    $("#homeSearch").css("height", searchHeight + "px");
});

if ($(".searchdate").length > 0) {
    $(function () {
        $(".searchdate").datepicker({
            monthNamesShort: $.datepicker.regional["tr"].monthNames,
            minDate: +0,
            onSelect: function () {
                var start_date = $(".searchdate").val();
                var a = start_date.substring(0, 2);
                var b = start_date.substring(3, 5);
                var c = start_date.substring(6, 10);
                var start_date = new Date(c + "-" + b + "-" + a);
                var end_date = new Date(c + "-" + b + "-" + a);
                if ($(".enddate").val() == "" || start_date > end_date) {
                    var start_time = new Date(start_date);
                    start_time.setDate(start_time.getDate() + 1);
                    var ay = ((start_time.getMonth() + 1).toString().length == 1) ? "0" + (start_time.getMonth() + 1).toString() : (start_time.getMonth() + 1).toString();
                    var gun = (start_time.getDate().toString().length == 1) ? "0" + start_time.getDate().toString() : start_time.getDate().toString();
                    $(".enddate").val(gun + "." + ay + "." + start_time.getFullYear());
                }
            },
            onClose: function (selectedDate) {
                $(".enddate").datepicker("option", "minDate", selectedDate).focus();
            }
        });
        $(".enddate").datepicker({ minDate: 0, monthNamesShort: $.datepicker.regional["tr"].monthNames });
    });
}

$('.btn-number').click(function (e) {
    e.preventDefault();
    var fieldName = $(this).attr('data-field');
    var type = $(this).attr('data-type');
    var input = $("input[name='" + fieldName + "']");
    var currentVal = (isNaN(parseInt(input.val())) ? 0 : parseInt(input.val()));
    if (!isNaN(currentVal)) {
        if (type == 'minus') {
            var minValue = parseInt(input.attr('min'));
            if (!minValue) minValue = 0;
            if (currentVal > minValue) {
                input.val(currentVal - 1).change();
            }
            if (parseInt(input.val()) == minValue) {
                $(this).attr('disabled', true);
            }
        } else if (type == 'plus') {
            var maxValue = parseInt(input.attr('max'));
            if (!maxValue) maxValue = 9999999999999;
            if (currentVal < maxValue) {
                input.val(currentVal + 1).change();
            }
            if (parseInt(input.val()) == maxValue) {
                $(this).attr('disabled', true);
            }
        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function () {
    $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function () {
    var minValue = parseInt($(this).attr('min'));
    var maxValue = parseInt($(this).attr('max'));
    if (!minValue) minValue = 0;
    if (!maxValue) maxValue = 9999999999999;
    var valueCurrent = parseInt($(this).val());
    var name = $(this).attr('name');
    if (valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if (valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
});


if ($("#slider-range").length > 0) {
    $(function () {
	$("#slider-range").slider({
		range: true,
		min: 200,
		max: 4500,
		values: [200, 1000],
		slide: function (event, ui) {
			$("#newsearchprice").val(ui.values[0] + " TL" + " - " + ui.values[1] + " TL");
		}
	});
	$("#newsearchprice").val($("#slider-range").slider("values", 0) + " TL" + " - " + $("#slider-range").slider("values", 1) + " TL");
});
}
 
if ($(".detailPage").length > 0) {
	$(window).scroll(function(event) {
		var scrll = Math.round($(this).scrollTop());
		var bottomobject = Math.round($(".reservation-section").offset().top); 
		var footer = Math.round($(".villa-transportation").offset().top); 
		if (scrll > bottomobject) {
			$(".make-reservation").addClass("fixed");
		} 
		if (scrll < bottomobject) {
			$(".make-reservation").removeClass("fixed");
		} 
		if (scrll > footer) {
			$(".make-reservation").removeClass("fixed");
		} 
	});
}

if ($(".detailPage").length > 0) {
	$(window).scroll(function(event) {
		var scrll = Math.round($(this).scrollTop());
		var bottomobject3 = Math.round($(".reservation-left-box").offset().top) - 40;
		var bottomobject4 = Math.round($("#scrollReservation").offset().top) - 70;
		if (scrll > bottomobject3) {
			$(".scrollReservationBtn").addClass("fixed");
		} else {
			$(".scrollReservationBtn").removeClass("fixed");
		}
		if (scrll > bottomobject4) {
			$(".scrollReservationBtn").removeClass("fixed");
		}
	});
}

 $(".scrollReservationBtn").click(function(e) {
        e.preventDefault();
        $(this).removeClass("fixed");
        var rzvForm = $(".make-reservation-box").offset().top;
        $('html, body').animate({
            scrollTop: rzvForm - 20
        }, 1500);
    });
 
function openNav() {
  document.getElementById("mySidenav").style.width = "300px";
  document.getElementById("main").style.marginLeft = "300px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}

if ($(".homePage").length > 0) {
	$(window).scroll(function(event) {
		var scrll = Math.round($(this).scrollTop());
		var bottomobject = Math.round($(".main").offset().top) - 185; 
		if (scrll > bottomobject) {
			$(".searchBox").addClass("fixed");
		} else {
			$(".searchBox").removeClass("fixed");
		}
	});
}
 
document.getElementById('mobile-filter-btn').onclick = function(){
    var el = document.getElementById('mobile-filter');
    if ( el.style.display != 'none' ){
        el.style.display = 'none';
    }
    else {
        el.style.display = 'block';
    };
}; 
